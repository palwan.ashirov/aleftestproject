import { createRouter, createWebHistory } from 'vue-router'
import FromCreate from '@/views/FromCreate.vue'
import FormPreview from '@/views/FormPreview.vue'

const routes = [
  {
    path: '/',
    name: 'FromCreate',
    component: FromCreate
  },
  {
    path: '/FormPreview',
    name: 'FormPreview',
    component: FormPreview
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
