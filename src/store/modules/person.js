export default {
  namespaced: true,
  state: { person: [] },

  mutations: {
    setPerson(state, data) {
      state.person = data
    }
  },
  actions: {},
  getters: {
    getPerson(state) {
      return state.person
    }
  }
}
