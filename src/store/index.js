import { createStore } from 'vuex'
import person from './modules/person'
export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    person
  }
})
